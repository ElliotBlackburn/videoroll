#import "VideoRoll.h"

@implementation VideoRoll

static NSString *videoPickedEvent = @"videoPickedEvent";
static NSString *batchVideoImageEvent = @"batchVideoImageCompleteEvent";
static NSString *batchVideoImageCreatedEvent = @"batchVideoImageCreatedEvent";
static NSString *batchVideoStartEvent = @"batchVideoStartEvent";

bool allowsTrim;
bool usesBytes;
NSString * videoTitle;
NSData * videoData;
UIImage * videoThumbnail;
AVURLAsset * videoCache;
UIPopoverController * popoverController;

NSTimeInterval timeStamp;

bool cacheAsset;
int32_t imageCounter;



-(void) setContext:(FREContext)ctx
{
    _context = ctx;
}

-(void) openVideoRoll:(const uint8_t *) pickerTitle
        withTitleLen:(uint32_t) titleLen
        withUseBytes:(BOOL) useBytes
        withAllowTrim:(BOOL) allowTrim
        withTrimLen:(int32_t) trimLen
        withCacheAsset:(BOOL) doCacheAsset;
{
    allowsTrim = allowTrim;
    usesBytes = useBytes;
    cacheAsset = doCacheAsset;
    videoTitle = [self uintToNSString: pickerTitle withLen: titleLen];
    
    UIImagePickerController * picker= [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    
    picker.allowsEditing = allowsTrim;
    if(allowsTrim && (trimLen != -1)){
        [picker setVideoMaximumDuration:trimLen];
    }

    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){

        popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
        popoverController.delegate = self;
        UIView * view = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
        [popoverController presentPopoverFromRect:CGRectMake(view.bounds.size.width/2, view.bounds.size.height/2, 1, 1) inView:view permittedArrowDirections:0 animated:YES];

    } else {

        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:picker animated:YES completion:NULL];
    }
    
    [picker release];
    
}

- (void) navigationController:(UINavigationController *) navigationController
        willShowViewController:(UIViewController *) viewController
        animated:(BOOL)animated
{
    [viewController.navigationItem setTitle: videoTitle];
}

- (void) imagePickerController:(UIImagePickerController *) picker
        didFinishPickingMediaWithInfo:(NSDictionary *) info
{
    
    NSURL * urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
    NSString *urlString=[urlvideo path];
    
    //if useBytes we will send back the binary video data along with a thumbnail
    if(usesBytes){
        //video byte array
        videoData = [[NSData alloc] initWithContentsOfURL:urlvideo];
        //get a thubnail from the selected video
        AVURLAsset *video = [[AVURLAsset alloc] initWithURL:urlvideo options:nil];
        AVAssetImageGenerator *assetGenerator = [[AVAssetImageGenerator alloc] initWithAsset:video];
        assetGenerator.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 600);
        NSError *error = nil;
        CMTime actualTime;
        CGImageRef image = [assetGenerator copyCGImageAtTime:time actualTime:&actualTime error:&error];
        videoThumbnail = [[UIImage alloc] initWithCGImage:image];
        CGImageRelease(image);

        [video release];
        [assetGenerator release];
        
    }

    //hold onto the AVAsset so it can be accesed later
    if(cacheAsset){
        videoCache = [[AVURLAsset alloc] initWithURL:urlvideo options:nil];
    }

    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        [popoverController dismissPopoverAnimated:YES];
        popoverController = nil;
    } else {
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    NSString * eventResponse = [self buildResponse: urlString withUrl:urlvideo];
    
    FREDispatchStatusEventAsync(_context, (uint8_t*)[videoPickedEvent UTF8String], (uint8_t*)[eventResponse UTF8String]);
}

-(void) acquireVideoBinaryData:(FREObject) videoByteArray withBitmmapData:(FREObject) thumbBitmapData;
{
    [self uiImageToBitmapData: thumbBitmapData withUIImage: videoThumbnail];
    
    [videoThumbnail release];

    //set the length on the ba
    FREObject baLength;
    FRENewObjectFromUint32(videoData.length, &baLength);
    
    FRESetObjectProperty(videoByteArray, (uint8_t *)"length", baLength, NULL);
    //get the bytearray
    FREByteArray videoBa;
    FREAcquireByteArray(videoByteArray, &videoBa);
    memcpy(videoBa.bytes, videoData.bytes, videoData.length);
    FREReleaseByteArray(videoByteArray);
    
    [videoData release];
    
}


-(void) getBitmapFromVideoAtTime:(double) time
        withBitmapData:(FREObject) bitmapData
        doCleanup:(BOOL) cleanup
{

    NSLog(@"Getting frame at: %f", time);
    AVAssetImageGenerator *assetGenerator = [[AVAssetImageGenerator alloc] initWithAsset:videoCache];
    assetGenerator.appliesPreferredTrackTransform = YES;
    CMTime cTime = CMTimeMakeWithSeconds(time, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGenerator copyCGImageAtTime:cTime actualTime:&actualTime error:&error];
    videoThumbnail = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    [assetGenerator release];
    
    [self uiImageToBitmapData: bitmapData withUIImage: videoThumbnail];
    [videoThumbnail release];
    if(cleanup){
        [videoCache release];
    }
    
}


-(void) getBitmapFromVideoAtTimes:(FREObject) timesArray
        thumbWidth:(double) width
        thumbHeight:(double) height
{

    uint32_t timesLen;
    FREGetArrayLength(timesArray, &timesLen);
    NSLog(@"Times len, %d width, %f height, %f",timesLen, width, height);
    NSMutableArray *thumbTimes=[NSMutableArray arrayWithCapacity:timesLen];


    for(int32_t i=0 ; i < timesLen - 1 ; i++) {
        FREObject currentElement;
        FREGetArrayElementAt(timesArray, i, &currentElement);
        double currentTime;
        FREGetObjectAsDouble(currentElement, &currentTime);
        CMTime indexTime = CMTimeMakeWithSeconds(currentTime, 30);

        NSValue *v=[NSValue valueWithCMTime:indexTime];
        [thumbTimes addObject:v];

    }

//    thumbImagesForZip = [[NSMutableArray alloc] init];

    AVAssetImageGenerator * generator = [[AVAssetImageGenerator alloc] initWithAsset:videoCache];

    imageCounter = 0;

    timeStamp = [[NSDate date] timeIntervalSince1970];

    NSString * batchStart = [NSString stringWithFormat:@"{\"videoStart\":\"%f\"}", timeStamp];
    FREDispatchStatusEventAsync(_context, (uint8_t*)[batchVideoStartEvent UTF8String], (uint8_t*)[batchStart UTF8String]);

    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){

        NSString *fileName = [NSString stringWithFormat:@"img_%d.png", imageCounter];

        NSLog(@"Attempting create image with filename");
        NSLog(fileName);

        NSString * folderName = [NSString stringWithFormat:@"%f", timeStamp];

        NSString *createdUrl = [self saveImgToTempDir:im withImgName:fileName withDirName:folderName];

//        [thumbImagesForZip addObject:[[NSBundle mainBundle] pathForResource:createdUrl ofType:@"png"]];

        NSString *videoProgress = [NSString stringWithFormat:@"{\"videoProgress\":\"%@\"}", createdUrl];

        FREDispatchStatusEventAsync(_context, (uint8_t*)[batchVideoImageCreatedEvent UTF8String], (uint8_t*)[videoProgress UTF8String]);

        double imgReqTime = CMTimeGetSeconds(requestedTime);
        imageCounter++;
        NSLog(@"generated thumb at time: %f %d of %d", imgReqTime, imageCounter, [thumbTimes count]);
        if(imageCounter == [thumbTimes count]){
            NSLog(@"images complete");
            NSString *complete = [NSString stringWithFormat:@"{\"videoComplete\":\"%d\"}", imageCounter];
            FREDispatchStatusEventAsync(_context, (uint8_t*)[batchVideoImageEvent UTF8String], (uint8_t*)[complete UTF8String]);
            [videoCache release];
            [generator release];
        }

    };
    generator.maximumSize = CGSizeMake(width, height);

    [generator generateCGImagesAsynchronouslyForTimes:thumbTimes completionHandler:handler];
}


-(NSString *) buildResponse:(NSString *) videoUrl withUrl:(NSURL *) nsUrl
{

    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:nsUrl];
    CMTime duration = playerItem.duration;

    return [NSString stringWithFormat:@"{\"sendBytes\":\"%d\",\"videoUrl\":\"%@\",\"width\":\"%f\",\"height\":\"%f\",\"length\":\"%f\"}",
            usesBytes, videoUrl, videoThumbnail.size.width, videoThumbnail.size.height, CMTimeGetSeconds(duration)];
    
}

-(NSString *)uintToNSString:(const uint8_t *) str withLen:(uint32_t) len
{
    return [[[NSString alloc] initWithBytes:str length:len encoding:NSUTF8StringEncoding] autorelease];
    
}

-(void) uiImageToBitmapData:(FREObject) thumbBitmapData withUIImage:(UIImage *) image
{
    CGImageRef ref = [image CGImage];
    [self uiImageToBitmapDataWithRef:thumbBitmapData withCGImgRef:ref];
}


-(void) uiImageToBitmapDataWithRef:(FREObject) thumbBitmapData withCGImgRef:(CGImageRef) ref
{

    NSLog(@"uiImageToBitmapDataWithRef: before FREAcquireBitmapData");

    //image bitmap
    FREBitmapData thumbBitmap;
    FREAcquireBitmapData(thumbBitmapData, &thumbBitmap);
    NSLog(@"uiImageToBitmapDataWithRef: after FREAcquireBitmapData width: %d height: %d", thumbBitmap.width, thumbBitmap.height);
    //get the image ref
    //get the size
    NSUInteger width = CGImageGetWidth(ref);
    NSUInteger height = CGImageGetHeight(ref);
    NSLog(@"after height");

    //need to get the colorspace to apply to bitmapdata
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSLog(@"after color space");
    //raw image data, width * height * 4 bytes per pixel
    unsigned char *imgData = malloc(width * height * 4);
    NSLog(@"after alloc imgData");
    //create bitmap context
    CGContextRef context = CGBitmapContextCreate(imgData, width, height, 8, (4 * width), colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    NSLog(@"after cgcontextfre");
    //cleanup
    CGColorSpaceRelease(colorSpace);
    NSLog(@"after release colorspace");
    //draw the image
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), ref);
    NSLog(@"after draw");
    //cleanup
    CGContextRelease(context);
    NSLog(@"after release context");
    
    //we need to loop over each pixel and write it to the acquired bitmap data object
    int x, y;
    //ty to freshplanet
    //https://github.com/freshplanet/ANE-ImagePicker/blob/master/ios/AirImagePicker/AirImagePicker.m

    int offset = thumbBitmap.lineStride32 - thumbBitmap.width;
    int offset2 = (4 * width) - thumbBitmap.width*4;
    int byteIndex = 0;
    uint32_t *bitmapDataPixels = thumbBitmap.bits32;
    for (y=0; y<thumbBitmap.height; y++){
        for (x=0; x<thumbBitmap.width; x++, bitmapDataPixels++, byteIndex += 4){
            
            int red = (imgData[byteIndex]);
            int green = (imgData[byteIndex + 1]);
            int blue = (imgData[byteIndex + 2]);
            int alpha = (imgData[byteIndex + 3]);
            // Combine values into ARGB32
            *bitmapDataPixels = (alpha << 24) | (red << 16) | (green << 8) | blue;
        }
        bitmapDataPixels += offset;
        byteIndex += offset2;
    }
    NSLog(@"after write loop");
    free(imgData);
    //invalidate bitmap
    FREInvalidateBitmapDataRect(thumbBitmapData, 0, 0, thumbBitmap.width, thumbBitmap.height);
    NSLog(@"after invalidate");
    //give it back to flash
    FREReleaseBitmapData(thumbBitmapData);
    NSLog(@"after release");

}

-(NSString *) saveImgToTempDir: (CGImageRef) image
                withImgName: (NSString *) name
                withDirName: (NSString *) dirName
{
    UIImage * uiImage = [UIImage imageWithCGImage:image];

    NSData * data = UIImagePNGRepresentation(uiImage);

    NSString* path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];

    path = [path stringByAppendingPathComponent: dirName];

    NSFileManager *fm = [[NSFileManager alloc] init];


    NSString * filePath = [path stringByAppendingPathComponent:name];

    if( ![fm fileExistsAtPath:path] ){
        [fm createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:NULL];
    }

    NSLog(@"Writing to path(saveImgToTempDir):");
    NSLog(path);

    [fm createFileAtPath:filePath contents:data attributes:nil];

    [fm release];

    return filePath;
}


@end
